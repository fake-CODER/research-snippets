"""
Polynomial system constructor for the 'Scalable Encryption Algorithm' defined in:

   Francois-Xavier Standaert, Gilles Biret, Neil Gershenfeld, and
   Jean-Jacques Quisquater. 'SEA: A Scalable Encryption Algorithm for
   Small Embedded Applications'.  in CARDIS 2006, 2006.

AUTHOR: Martin Albrecht <malb@informatik.uni-bremen.de>
"""
from sage.crypto.mq.mpolynomialsystemgenerator import MPolynomialSystemGenerator

class SEA(MPolynomialSystemGenerator):
    """
    Multivariate polynomial system constructor for the scalable
    encryption algorithm as defined in

      Francois-Xavier Standaert, Gilles Biret, Neil Gershenfeld, and
      Jean-Jacques Quisquater. 'SEA: A Scalable Encryption Algorithm
      for Small Embedded Applications'.  in CARDIS 2006, 2006.

    """
    def __init__(self, n, b, nr, **kwargs):
        """
        Return a new constructor for a SEA polynomial system, i.e. this is a
        constructor to construct constructors for polynomial systems.

        INPUT:
            n -- blocksize / keysize in bits must be multiple of 6b
            b -- wordsize
            nr -- number of rows
            postfix -- a string which is appended to variable names before the indices (default: '')
            order -- a string to specify the term ordering of the variables

        """
        if n % (6*b) != 0:
            raise TypeError, "n must be multiple of 6*b"
        
        self._n = int(n)
        self._b = int(b)
        self._nb = int(int(n)/2*int(b))
        self._nr = int(nr)

        self._postfix = kwargs.get("postfix", "")
        self._order = kwargs.get("order", "degrevlex")
        self._base = GF(2)

    def __getattr__(self, attr):
        if attr == "n":
            return self._n
        elif attr == "b":
            return self._b
        elif attr == "nb":
            return self._nb
        elif attr == "nr":
            return self._nr
        elif attr == "R":
            self.R = self.ring()
            return self.R
        else:
            raise AttributeError

    def _varformatstr(self, name):
        l = str(max([len(str(self.nr)), len(str(self.n/2-1))]))
        if not name.startswith("K"):
            name += self._postfix
        return name + "%0" + l + "d" + "%0" + l + "d"

    def _varstrs(self, name, round):
        s = self._varformatstr(name)
        return [s%(round,i) for i in reversed(range(self.n/2)) ]

    def _vars(self, name, round):
        return [self.R(e) for e in self._varstrs(name,round)]

    def block_order(self):
        nr = self.nr
        n = self.n

        T = TermOrder('degrevlex',n)
        for _ in range(2,nr,2):
            T = TermOrder('degrevlex',n) + T
            T = TermOrder('degrevlex',n) + T

        return T

    def ring(self, order=None):
        """
        Return a ring to hold all the variables of the
        MPolynomialSystem of self.
        """
        if order is not None:
            self._order = order

        if self._order == 'block':
            self._order = self.block_order()
        
        names = []
        nr = self.nr

        names += [self._varformatstr("KR")%(0,j) for j  in reversed(range(self.n/2))]
        names += [self._varformatstr("KL")%(0,j) for j  in reversed(range(self.n/2))]

        for i in xrange(2,nr,2):
            names += [self._varformatstr("KR")%(i,j) for j  in reversed(range(self.n/2))]
            names += [self._varformatstr("KL")%(i,j) for j  in reversed(range(self.n/2))]
            names += [self._varformatstr("R")%(i,j) for j  in reversed(range(self.n/2))]
            names += [self._varformatstr("L")%(i,j) for j  in reversed(range(self.n/2))]

        return PolynomialRing(GF(2),len(names), names, order=self._order)

    def base_ring(self):
        """
        """
        return self._base

    def single_sbox_polynomials(self, l):
        """
        """
        x2,x1,x0 = l # big endian

        return [ x0*x1 + x0 + x1 + x2, x2*x1 + x2*x0 + x1, x2*x1 + x0 ]

    def sbox_polynomials(self,l):
        """
        """
        n = self.n
        b = self.b
        ret = [0]*(n/2)
        for i in xrange(n/2/3):
            ret[i],ret[i+b],ret[i+2*b] = self.single_sbox_polynomials([l[i],l[i+b],l[i+2*b]])
        return ret

##     def single_sbox_polynomials(self, x, y):
##         """
##         Return polynomials for one sbox.
        
##         INPUT:
##             x -- list/tuple of three input variables
##             y -- list/tupke of three output variables
##         """
##         x0,x1,x2 = x
##         y0,y1,y2 = y
        
##         return [y1*y0 + x2 + y2 + y1 + y0,
##                 y2*y0 + x1 + y1 + y0,
##                 x1*y0 + x2 + x1 + y2 + y0,
##                 x2*y0 + x1 + y1,
##                 y2*y1 + x1 + x0 + y0,
##                 x0*y1 + x0*y0 + x2 + x0 + y2 + y1 + y0,
##                 x1*y1 + x0*y0 + x1 + y0,
##                 x2*y1 + x1 + x0 + y1 + y0,
##                 x0*y2 + x1 + y1 + y0,
##                 x1*y2 + x1 + x0 + y0,
##                 x2*y2 + x0*y0 + x2 + x1 + x0 + y1,
##                 x1*x0 + x2 + x1 + x0 + y2,
##                 x2*x0 + x1 + x0 + y1 + y0,
##                 x2*x1 + x0 + y0]

##     def sbox_polynomials(self, x, y):
##         """
##         Return polynomials for the sboxes of self.nr/2 input/output
##         variables.

##         INPUT:
##             x -- list/tuple of input variables of length self.nr/2
##             y -- list/tuple of output variables of length self.nr/2
##         """
##         polys = []
##         for i in xrange(self.n/6):
##             polys += self.single_sbox_polynomials(x[3*i:3*i+3], y[3*i:3*i+3])
##         return polys

    def xor_polynomials(self, L, R, Y):
        """
        Return polynomials of the form y + r + l

        INPUT:
            l -- list/tuple of input variables of length self.nr/2
            r -- list/tuple of input variables of length self.nr/2
            y -- list/tuple of output variables of length self.nr/2
        """
        return [ Y[i] + L[i] + R[i] for i in xrange(self.n/2) ]

    def addition_polynomials(self, L, R, Y, C):
        """
        Return polynomials for the modular addtion mod $2^8$.

        INPUT:
            l -- list/tuple of input variables of length self.nr/2
            r -- list/tuple of input variables of length self.nr/2
            y -- list/tuple of output variables of length self.nr/2
            c -- list/tuple of carry variables of length self.nr - 2
        """
        return self.xor_polynomials(l,r,y)

##         b = self.b

##         polys = []
##         for i in xrange(self.n/2/b):
##             polys.append( l[i*b] + r[i*b] + y[i*b] )
##             polys.append( c[i*b] + l[i*b]*r[i*b])
##             for j in xrange(1,b):
##                 offset = i*b+j
##                 polys.append(l[offset] + r[offset] + y[offset] + c[offset-1])
##             for j in xrange(1,b-1):
##                 polys.append(c[offset] + l[offset]*r[offset] + l[offset]*c[offset-1] + r[offset]*c[offset-1] )
##         return polys

    def word_shift(self, x):
        """
        Shift words of x to the left by one word.

        INPUT:
            x -- list/tuple of input variables of length self.nr/2

        """
        v = Matrix(self.R,len(x),1,list(x))
        try:
            return (self._word_shift_matrix * v).list()
        except AttributeError:
            n2 = self.n/2
            b = self.b
            word_shift_matrix = Matrix(self.base_ring(), n2, n2)
            for i in xrange(n2):
                word_shift_matrix[ (i-b)%n2, i ] = 1
            self._word_shift_matrix = word_shift_matrix
            return (word_shift_matrix * v).list()

    def bit_shift(self, x):
        """
        Shift elements of x as follows:
        $x_{3i} = x_{3i} >>> 1$,
        $x_{3i+1} = x_{3i+1}$,
        $x_{3i+2} = x_{3i+2} <<< 1$, for  $0 <= i <= n_b/3 - 1$,

        INPUT:
            x -- list/tuple of input variables of length self.nr/2

        """
        v = Matrix(self.R, len(x), 1, list(x))
        try:
            return (self._bit_shift_matrix * v).list()
        except AttributeError:
            n2 = self.n/2
            b = self.b
            bit_shift_matrix = Matrix(GF(2), n2, n2)

            for i in xrange(n2/3/b):
                for j in xrange(b):
                    bit_shift_matrix[3*b*i       +j, 3*b*i       + (j+1)%b] = 1

                for j in xrange(b):
                    bit_shift_matrix[3*b*i +   b +j, 3*b*i +   b +       j] = 1

                for j in xrange(b):
                    bit_shift_matrix[3*b*i + 2*b +j, 3*b*i + 2*b + (j-1)%b] = 1
                    
            self._bit_shift_matrix = bit_shift_matrix
            return (bit_shift_matrix*v).list()

    def key_schedule_round_polynomials(self, i):
        if i%2:
            raise TypeError

        n2 = self.n/2
        nr = self.nr
        ring = self.R
        
        R = lambda x: Matrix(ring,n2,1,self.word_shift(x.list()))
        r = lambda x: Matrix(ring,n2,1,self.bit_shift(x.list()))
        S = lambda x: Matrix(ring,n2,1,self.sbox_polynomials(x.list()))

        C = lambda x: Matrix(ring,n2,1,self.C(x))

        KLi = Matrix(ring, n2,1,self._vars("KL",i-2))
        KRi = Matrix(ring, n2,1,self._vars("KR",i-2))

        KLi2 = Matrix(ring, n2,1,self._vars("KL",i))
        KRi2 = Matrix(ring, n2,1,self._vars("KR",i))

        if i <= self.nr//2:
            Ci = C(i)
            Ci1 = C(i-1)
        elif i == self.nr//2 + 1:
            Ci = C(nr-i)
            Ci1 = C(i-1)
        else:
            Ci = C(nr-i)
            Ci1 = C(nr-(i-1))

        if i == self.nr//2:
            # only happens if (nr-1)%4 == 0
            KRi2,KLi2 = KLi2,KRi2
        elif i == self.nr//2 + 1:
            lhs =  KLi2 + KRi
            rhs =  KRi2 + KLi + R(r(S(KRi + Ci))) + R(r(S(KRi + Ci1)))
            return lhs.list(),rhs.list()

        lhs =  KLi2 + KLi + R(r(S(KRi + Ci1)))
        rhs =  KRi2 + KRi + R(r(S(KLi2 + Ci)))
        return lhs.list(),rhs.list()

    def round_polynomials(self, i, P=None, C=None):
        nr = self.nr
        n2 = self.n/2
        ring = self.R

        if i%2 and i!=nr:
            raise TypeError
        
        R = lambda x: Matrix(ring,n2,1,self.word_shift(x.list()))
        r = lambda x: Matrix(ring,n2,1,self.bit_shift(x.list()))
        S = lambda x: Matrix(ring,n2,1,self.sbox_polynomials(x.list()))
        

        if i!=nr:
            if i == 2:
                Li  = Matrix(ring, n2, 1, P[:n2] )
                Ri  = Matrix(ring, n2, 1, P[n2:] )
            else:
                Li  = Matrix(ring, n2, 1, self._vars("L",i-2))
                Ri  = Matrix(ring, n2, 1, self._vars("R",i-2))
            
            Li2 = Matrix(ring, n2, 1, self._vars("L",  i))
            Ri2 = Matrix(ring, n2, 1, self._vars("R",  i))

            if (nr -1)%4 != 0:
                if i < nr//2 + 1:
                    KRi = Matrix(ring, n2, 1, self._vars("KR",i-2))
                    KLi = Matrix(ring, n2, 1, self._vars("KL",i))
                elif i == nr//2  + 1:
                    KLi2 = Matrix(ring, n2, 1, self._vars("KL",i-2))
                    KRi2 = Matrix(ring, n2, 1, self._vars("KR",i-2))
                    Ci1 = Matrix(ring,n2,1,self.C(i-1))

                    KRi = Matrix(ring, n2, 1, self._vars("KR",i-2))
                    KLi =  KLi2 + R(r(S(KRi2 + Ci1)))
                    
                else:
                    KRi = Matrix(ring, n2, 1, self._vars("KL",i-2))
                    KLi = Matrix(ring, n2, 1, self._vars("KR",i-2))
            else:
                if i < nr//2:
                    KRi = Matrix(ring, n2, 1, self._vars("KR",i-2))
                    KLi = Matrix(ring, n2, 1, self._vars("KL",i))
                elif i == nr//2:
                    KRi = Matrix(ring, n2, 1, self._vars("KR",i-2))
                    KLi = Matrix(ring, n2, 1, self._vars("KR",i))
                else:
                    KRi = Matrix(ring, n2, 1, self._vars("KL",i-2))
                    KLi = Matrix(ring, n2, 1, self._vars("KR",i-2))                

            lhs = Li2 + R(Li) + r(S(Ri + KRi))
            rhs = Ri2 + R(Ri) + r(S(Li2 + KLi))
            return lhs.list(),rhs.list()

        else:
            Li = Matrix(ring, n2,1,self._vars("L",i-1))
            Ri = Matrix(ring, n2,1,self._vars("R",i-1))
            Li1 = Matrix(ring, n2, 1, C[n2:])
            Ri1 = Matrix(ring, n2, 1, C[:n2])

            KLi = Matrix(ring, n2,1,self._vars("KL",i-1))

            lhs = Li1 + Ri
            rhs = Ri1 + R(Li) + r(S(Ri + KLi))
            return lhs.list(),rhs.list()

    def polynomial_system(self, P=None, K=None):
        """
        Return a polynomial system for self and P,K so as a solution
        to the system.

        INPUT:
            P -- plaintext (default: random)
            K -- key (default: random)

        OUTPUT:
            MPolynomialSystem, dictionary of K0 solutions
        """
        if not P:
            P = self.random_element()
        if not K:
            K = self.random_element()
        
        C,s2 = self(P,K)
        
        nr = self.nr
        l = []
        # strangely enough, if I add these GB calculations become slower
        fp = [x**2 + x for x in self._vars("KR",0) + self._vars("KL",0)]
        l.append(mq.MPolynomialRoundSystem(self.R, fp))

        for i in xrange(2, nr, 2):
            lhs,rhs = self.key_schedule_round_polynomials(i)
            fp = [x**2 + x for x in self._vars("KR",i) + self._vars("KL",i)]
            l.append(mq.MPolynomialRoundSystem(self.R, lhs + rhs + fp ))
        
            lhs,rhs = self.round_polynomials(i,P,C)
            fp = [x**2 + x for x in self._vars("R",i) + self._vars("L",i)]
            l.append(mq.MPolynomialRoundSystem(self.R, lhs + rhs + fp ))

        lhs, rhs = self.round_polynomials(self.nr,P,C)

        l.append(mq.MPolynomialRoundSystem(self.R,lhs + rhs ))

        s = dict(zip(self._vars("KL",0) + self._vars("KR",0), K))

        return mq.MPolynomialSystem(self.R,l), s2


    #### encrytion routines

    def __call__(self, P, K):
        """
        """
        nr = self.nr
        KL = [[]]*nr
        KR = [[]]*nr

        C = self.C

        L = [[]]*(nr+1)
        R = [[]]*(nr+1)
        L[0] = P[0:self.n/2]
        R[0] = P[self.n/2:self.n]

        KL[0],KR[0] = K[:self.n/2], K[self.n/2:]

        # key schedule
        for i in xrange(1, self.nr//2 + 1):
            KL[i], KR[i] = self.FK(KL[i-1],KR[i-1],self.C(i))

        KL[i], KR[i] = KR[i], KL[i]

        for i in xrange(self.nr//2 + 1, self.nr):
            KL[i], KR[i] = self.FK(KL[i-1],KR[i-1],self.C(self.nr-i))

        # encryption
        for i in xrange(1, self.nr//2 + 1):
            L[i], R[i] = self.FE(L[i-1], R[i-1], KR[i-1])
            #print hex_str(self,L[i]),hex_str(self,R[i])

        for i in xrange(self.nr//2 + 1, self.nr+1):
            L[i], R[i] = self.FE(L[i-1], R[i-1], KL[i-1])
            #print hex_str(self,L[i]),hex_str(self,R[i])

        solutions = []

        solutions += zip(self._vars("KL",0),KL[0])
        solutions += zip(self._vars("KR",0),KR[0])

        for i in xrange(2,self.nr,2):
            #solutions += zip(self._vars("KL",i),KL[i])
            #solutions += zip(self._vars("KR",i),KR[i])
            #solutions += zip(self._vars("L",i),L[i])
            #solutions += zip(self._vars("R",i),R[i])
            pass
            
        return R[nr] + L[nr] , dict(solutions)

    def C(self, i):
        """
        """
        l = [0 for _ in xrange(self.n/2)]
        j = self.n/2 - 1
        while i:
            if i%2:
                l[j] = 1
            j -= 1
            i = i>>1
        return [self.base_ring()(e) for e in l]

    def FE(self,L,R,Ki):
        Y = self.addition(R,Ki)
        Y = self.S(Y)
        Y = self.bit_shift(Y)
        Y = [self.word_shift(L)[i] + Y[i] for i in xrange(self.n/2) ]
        return R,Y

    def FK(self,KL,KR,C):
        Y = self.addition(KR, C)
        Y = self.S(Y)
        Y = self.bit_shift(Y)
        Y = self.word_shift(Y)
        Y = [KL[i] + Y[i] for i in xrange(self.n/2)]
        return KR,Y

    def S(self,Y):
        n = self.n
        b = self.b
        k = self.base_ring()

        l = [0]*(n/2)
        _S = {(0,0,0):(0,0,0),\
              (0,0,1):(1,0,1),\
              (0,1,0):(1,1,0),\
              (0,1,1):(1,1,1),\
              (1,0,0):(1,0,0),\
              (1,0,1):(0,1,1),\
              (1,1,0):(0,0,1),\
              (1,1,1):(0,1,0)}
        for i in xrange(0,n/2/3):
            l[i],l[i+b],l[i+2*b] = _S[int(Y[i]),int(Y[i+b]),int(Y[i+2*b])]
            
        return l

    def addition(self, left, right):
        return [left[i] + right[i] for i in xrange(self.n/2) ]
        k = self.base_ring()
        b = self.b
        l = []
        for i in xrange(0,self.n/2/b):
            res = ( ZZ(map(ZZ,left[b*i:b*i+b]),2) + ZZ(map(ZZ,right[b*i:b*i+b]),2)) % 2**b
            res = [k(e) for e in res.digits()]
            if len(res) < b:
                res = [k(0)]*(b-len(res)) + res
            l += res
        return l

    def random_element(self):
        return [self.base_ring().random_element() for _ in xrange(self.n)]


def sea_big_ring(self,samples=1):
    names = []
    nr = self.nr

    names += [self._varformatstr("KR")%(0,j) for j  in reversed(range(self.n/2))]
    names += [self._varformatstr("KL")%(0,j) for j  in reversed(range(self.n/2))]

    for i in xrange(2,nr,2):
        for sample in xrange(samples):
            names += [self._varformatstr("R%d"%sample)%(i,j) for j  in reversed(range(self.n/2))]
            names += [self._varformatstr("%d"%sample)%(i,j) for j  in reversed(range(self.n/2))]
        names += [self._varformatstr("KR")%(i,j) for j  in reversed(range(self.n/2))]
        names += [self._varformatstr("KL")%(i,j) for j  in reversed(range(self.n/2))]

    return PolynomialRing(self.base_ring(),len(names), names)

def sea_big_system(self, samples=1):
    P = sea_big_ring(self,samples)
    K = self.random_element()
    l = []
    orig_l = []
    for sample in xrange(samples):
        sr = SEA(self.n,self.b,self.nr, postfix=str(sample))
        F,s = sr.polynomial_system(K=K)
        orig_l.append(F)
        l.append([P(str(e)) for e in F.gens() ])
    
    return MPolynomialSystem(P,l)

def hex_str(sea, l):
    ret = []
    b = sea.b
    for i in range(len(l)/b):
        ret.append(  hex(int("".join(map(str,l[i*b:i*b+b])),2)) )
    return " ".join(ret)



