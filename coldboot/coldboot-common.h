#ifndef COMMON__H
#define COMMON__H

typedef unsigned int u32;
typedef unsigned char u8;
#define le32_to_cpu __le32_to_cpu


/* Hamming Weight */
#define hw(b) b = b - ((b >> 1) & 0x55555555); \
              b = (b & 0x33333333) + ((b >> 2) & 0x33333333); \
              b = (((b + (b >> 4) & 0xF0F0F0F) * 0x1010101) >> 24); 
#define rol32(word, shift) ((word << shift) | (word >> (32 - shift)))
#define ror32(word, shift) ((word >> shift) | (word << (32 - shift)))

static u32 randomize_word(u32 word, double prob, int xor) {
  int i;
  u32 err = 1;
  long threshold = (long)((1UL<<31) * prob);
  long threshold_small = (long)((1UL<<31) * 0.001);

  if (xor) {
    for(i=0; i<32; i++) {
      if (random() < threshold) {
	word ^=err;
      }
      err <<= 1;
    }
  } else {
    for(i=0; i<32; i++) {
      if (random() < threshold_small) {
	word &= ~err;
      } else if (random() < threshold) {
	word |=err;
      }
      err <<= 1;
    }
  }
  return word;
}

#endif //COMMON__H
