from itertools import cycle
from sage.misc.cachefunc import cached_method
from sage.all import *
from anf2mip import *

class SerpentColdboot:
    GF2 = GF(2)
    VS = VectorSpace(GF(2),32)
    phi = VS(list(reversed( ZZ(0x9e3779b9).digits(2) )))

    @classmethod
    def expand_i(CTX, i):
        i = list(reversed(ZZ(i).digits(2,padto=32)))
        return CTX.VS(map(SerpentColdboot.GF2,i))

    @classmethod
    def w(CTX, W, i):
        _w = W[i-8] + W[i-5] + W[i-3] +  W[i-1] + CTX.phi + CTX.expand_i(i-8)
        return _w.parent()(_w[11:].list() + _w[:11].list())

    @classmethod
    def prekey(CTX, W, Z=False):
        W = copy(W)
        for i in xrange(8,132+8):
            W.append(CTX.w(W,i))
        return W

    def random_key(self):
        return [self.GF2.random_element() for _ in range(self.key_size)] \
            + [self.GF2.one_element()] \
            + [self.GF2.zero_element() for _ in range(256-self.key_size-1)]

    def __init__(self, nwords, key_size=128):
        assert(nwords%4 == 0 and nwords > 0)
        assert(key_size in (128,192,256))
        self.nwords = nwords
        self.key_size = key_size

    @cached_method
    def ring(self):
        nwords = self.nwords
        K = ["K%03d"%i for i in range(256)]
        w = ["w%04d"%i for i in range(nwords*32)] # prekey
        k = ["k%04d"%i for i in range(nwords*32)]
        #e = ["e%04d"%i for i in range(nwords*32)]

        B = BooleanPolynomialRing(len(K+w+k),K+w+k,order="lex")

        gd = B.gens_dict()
        f = lambda x: gd[x]

        self.k_var = map(f, k)
        self.w_var = map(f, w)
        self.K_var = map(f, K)
        return B

    @cached_method
    def vars_dict(self):
        B = self.ring()
        return B.gens_dict()

    @cached_method
    def free_module(self):
        return FreeModule(self.ring(), 32)


    def prekey_polynomials(self, W):
        W = copy(W)
        l = []

        FM = self.free_module()

        for i in xrange(self.nwords):
            _W = self.w(W,i+8)
            w = FM(self.w_var[32*i:32*i+32])
            W.append(w)
            l.extend((w + _W).list())
        return l

    def polynomial_system(self, K=None, delta0=0.0, delta1=0.0):
        B = self.ring()
        FM = self.free_module()

        K_val = [SerpentColdboot.GF2.random_element() for _ in range(256)] if (K is None) else K
        K_val = [SerpentColdboot.VS(K_val[32*i:32*i+32]) for i in range(8)]
        w_val = self.prekey(K_val)

        H = []
        if self.key_size < 256:
            H.append(self.K_var[self.key_size] + 1)

        for i in range(self.key_size+1,256):
            H.append(self.K_var[i])

        K_var = [FM(self.K_var[32*i:32*i+32]) for i in range(8)]
        H.extend(self.prekey_polynomials(K_var))

        w_var = [FM(self.w_var[32*i:32*i+32]) for i in range(self.nwords)]
        k_var = [FM(self.k_var[32*i:32*i+32]) for i in range(self.nwords)]


        it = cycle([3,2,1,0,7,6,5,4])

        S = []

        for i in range(0, self.nwords,4):
            sbox = SerpentSBox(it.next())
            
            for j in range(32):
                y = sbox([w_val[i+8+0][j], w_val[i+8+1][j], w_val[i+8+2][j], w_val[i+8+3][j]])

                for ii in range(4):
                    if random() <= delta0 and y[ii] == 1:
                        y[ii] -= 1
                    if random() <= delta1 and y[ii] == 0:
                        y[ii] += 1

                X = w_var[i+0][j], w_var[i+1][j], w_var[i+2][j], w_var[i+3][j]
                Y = k_var[i+0][j], k_var[i+1][j], k_var[i+2][j], k_var[i+3][j]

                H.extend(sbox_eq(sbox, X,Y))
                S.extend([Y[ii] + y[ii] for ii in range(4)])
        s = {}
        for i in range(8):
            for j in range(32):
                s[K_var[i][j]] = K_val[i][j]

        return ProbabilisticMPolynomialSystem(B, hard=H, soft=S),s 

    def key_schedule(self, K=None):
        K = [SerpentColdboot.GF2.random_element() for _ in range(256)] if (K is None) else K
        K = [SerpentColdboot.VS(K[32*i:32*i+32]) for i in range(8)]
        w = self.prekey(K)

        it = cycle([3,2,1,0,7,6,5,4])

        K = [self.VS(0) for _ in range(self.nwords)]
        for i in range(0, self.nwords,4):
            sbox = SerpentSBox(it.next())
            for j in range(32):
                y = sbox([w[i+8+0][j], w[i+8+1][j], w[i+8+2][j], w[i+8+3][j]])
                K[i+0][j] = y[0]
                K[i+1][j] = y[1]
                K[i+2][j] = y[2]
                K[i+3][j] = y[3]

        return K
        

class SerpentSBox(mq.SBox):
    def __init__(self, i):
        if 0 <= i < 8: 
            self._i = i
        else:
            raise TypeError, "Expected 0 <= i < 8, but got i = %s."%i
        self.m = 4
        self.n = 4
        self._F = GF(2)
        self._big_endian = True

    def __cmp__(self, other):
        if isinstance(other, (int,Integer,long)):
            return cmp(self._i, other)
        else:
            return mq.SBox.__cmp__(self, other)

    def __call__(self, x):
        to_int = False

        if isinstance(x, (Integer, int, long)):
            x = self.to_bits(x)
            to_int = True

        y = S(self._i, *x)
        y = map(lambda x: int(x%2), y)
        if to_int:
            return self.from_bits(y)
        else:
            return y


def sbox_eq(i, X, Y):
    x0,x1,x2,x3 = X
    y0,y1,y2,y3 = Y

    if i == 0:
        return [x0 + y0*y1*y3 + y0*y1 + y0*y2*y3 + y0*y3 + y1*y2*y3 + y1*y2 + y1*y3 + y2*y3 + y2 + 1, 
                x1 + y0*y2*y3 + y0*y2 + y0 + y1*y2*y3 + y1*y3 + y1 + y2, 
                x2 + y0*y1 + y0 + y1 + y2 + y3 + 1, 
                x3 + y0*y1*y3 + y0*y2*y3 + y0 + y1*y2*y3 + y1*y2 + y2*y3 + y3 + 1]

        # return [x0*x1 + x0*x2 + x0*y3 + x0,
        #         x0*x1 + x1*x3 + x2 + x3 + y0 + y1,
        #         x0*x2 + x0*y0 + x1*y0 + x0*y1 + x0*y2 + x3 + y2,
        #         x0*x1 + x0*x2 + x1*x2 + x0*y0 + x0*y1 + x1*y1 + x0*y2 + x3 + y2,
        #         x0*x2 + x1*x2 + x0*y1 + x0*y2 + x1*y2 + y0 + y3 + 1,
        #         x0*x1 + x0*x2 + x1*x2 + x0*y0 + x0*y1 + x1*y3 + x0 + y0 + y1 + y3,
        #         x0*x3 + x0 + x1 + x2 + x3 + y3,
        #         x0*x2 + x1*x2 + x2*x3 + x2*y0 + x0*y1 + x0*y2 + x0 + x3 + y0 + y1 + y2 + y3,
        #         x1*x2 + x0*x3 + x0*y1 + x2*y1 + x0*y2 + x2,
        #         x0*x2 + x1*x2 + x2*x3 + x2*y2,
        #         x0*x1 + x0*x2 + x0*x3 + x2*x3 + x2*y3 + y1 + y2 + y3 + 1,
        #         x0*x1 + x0*x2 + x1*x2 + x2*x3 + x3*y0 + x0*y1 + x2 + y1 + y2 + y3 + 1,
        #         x0*x1 + x1*x2 + x0*x3 + x0*y0 + x3*y1 + y0 + y2 + y3 + 1,
        #         x0*x1 + x1*x2 + x0*x3 + x0*y2 + x3*y2 + x0 + x2 + y0 + 1,
        #         x0*x1 + x2*x3 + x3*y3 + x2 + y0 + y1,
        #         y0*y1 + x2 + y0 + y1 + y2 + y3 + 1,
        #         x0*x1 + x0*x2 + x1*x2 + x0*x3 + x2*x3 + x0*y1 + x0*y2 + y0*y2 + x2 + x3 + y1 + y3 + 1,
        #         x0*x1 + x0*x3 + y0*y3 + x0 + x2 + y0 + y2 + 1,
        #         x0*x1 + x1*x2 + x0*y1 + x0*y2 + y1*y2 + x2 + x3 + y0 + y1 + y2,
        #         x0*x1 + x0*x3 + y1*y3 + x3 + y0 + y1 + y2,
        #         x0*x1 + x0*x2 + x2*x3 + x0*y0 + x0*y2 + y2*y3 + x0 + y1 + y2 + 1]
    elif i == 1:
        return [x0 + y0*y1*y2 + y0*y1 + y0*y2*y3 + y0 + y1*y2*y3 + y1*y3 + y1 + 1, 
                x1 + y0*y1*y2 + y0*y2*y3 + y0*y3 + y1*y2*y3 + y1*y3 + y1 + y2 + y3, 
                x2 + y0*y1*y2 + y0*y2*y3 + y0*y2 + y0 + y1*y2 + y1 + y3 + 1, 
                x3 + y0 + y1*y3 + y2 + y3]

        # return [x0*x2 + x0*x3 + x0*y2 + x0,
        #         x0*x2 + x1*x3 + x0*y2 + x3 + y1 + y2 + y3 + 1,
        #         x0*x1 + x1 + x2 + x3 + y2 + 1,
        #         x0*x2 + x0*x3 + x0*y0 + x1*y0 + x1*y1 + x0*y2 + x0*y3 + x1 + x2 + y2 + y3,
        #         x0*x2 + x1*x2 + x0*y2 + x1*y2 + x1 + x2 + y1 + y3,
        #         x1*x2 + x0*x3 + x0*y0 + x1*y0 + x0*y1 + x0*y2 + x1*y3 + x2 + x3 + y1 + 1,
        #         x1*x2 + x0*x3 + x2*x3 + x0*y1 + x0*y3 + x3 + y0 + y3,
        #         x0*x3 + x1*y0 + x2*y0 + x0*y3 + x1 + x2 + y0 + 1,
        #         x0*x3 + x0*y0 + x1*y0 + x0*y1 + x2*y1 + x0*y2 + x1 + x2 + y0 + y2 + y3 + 1,
        #         x1*x2 + x0*y0 + x1*y0 + x0*y2 + x2*y2 + x3 + y0 + y3,
        #         x0*x3 + x0*y1 + x0*y2 + x2*y3 + x3 + y0 + y2,
        #         x0*x2 + x1*y0 + x3*y0 + x0*y1 + x3 + y0 + y1 + y2 + 1,
        #         x1*y0 + x3*y1 + x0*y2 + x0*y3 + x1 + x3 + y0 + y1 + y2 + y3,
        #         x0*x2 + x1*x2 + x0*x3 + x3*y2 + y0 + y1 + y2 + 1,
        #         x1*x2 + x0*x3 + x1*y0 + x0*y3 + x3*y3 + x1 + y1 + y2,
        #         x0*x2 + y0*y1 + x0*y2 + x1 + x2 + x3 + y0 + y1 + y2,
        #         x0*x2 + x0*y0 + x1*y0 + x0*y2 + y0*y2 + x1 + x2 + x3 + y0 + y1 + y2,
        #         x0*x3 + y0*y3 + x2 + x3 + y1 + y3 + 1,
        #         x0*x2 + x1*y0 + y1*y2 + x0*y3 + x2 + y3,
        #         y1*y3 + x3 + y0 + y2 + y3,
        #         x1*x2 + x1*y0 + x0*y3 + y2*y3 + x1 + x2 + y0 + y3 + 1]
    elif i == 2:
        return [x0 + y0 + y1*y2 + y1*y3 + y1 + y2, 
                x1 + y0*y1*y3 + y0*y1 + y0*y2*y3 + y0*y3 + y1 + y2*y3 + y2, 
                x2 + y0*y1*y3 + y0*y1 + y0*y2*y3 + y0*y3 + y0 + y1*y3 + y2 + y3 + 1, 
                x3 + y0*y1*y2 + y0*y1 + y0*y2*y3 + y1*y2 + y3 + 1]

        # return [x0*x2 + x1 + x2 + x3 + y0,
        #         x0*x2 + x1*x3 + x0*y0 + x0*y2 + x3 + y0 + y3 + 1,
        #         x0*x2 + x1*x2 + x0*x3 + x1*y0 + y0 + y1 + y2,
        #         x0*x2 + x0*x3 + x0 + x2 + y0 + y1 + y2 + y3 + 1,
        #         x0*x2 + x1*x2 + x0*y1 + x1*y1 + x0*y2 + x1*y2,
        #         x1*x2 + x0*y0 + x1*y3 + x2 + x3 + y1 + y2,
        #         x0*x1 + x0*x3 + x0*y0,
        #         x0*x2 + x1*x2 + x2*x3 + x2*y0 + x2,
        #         x0*x3 + x0*y0 + x0*y1 + x0*y2 + x0*y3,
        #         x1*x2 + x2*x3 + x0*y1 + x1*y1 + x2*y2 + x2 + y1,
        #         x1*x2 + x2*x3 + x1*y1 + x2*y1 + x0*y2 + x2*y3 + x2 + x3 + y1 + y2 + y3 + 1,
        #         x1*x2 + x0*y0 + x3*y0 + x0*y1 + x2 + x3 + y0 + y2,
        #         x1*x2 + x0*x3 + x2*x3 + x1*y1 + x3*y1 + x0*y2 + x3 + y2,
        #         x0*x2 + x1*x2 + x0*x3 + x2*x3 + x1*y1 + x2*y1 + x0*y2 + x3*y2 + x3 + y1,
        #         x0*y0 + x2*y1 + x0*y2 + x3*y3 + x3 + y0 + y1 + y2 + y3 + 1,
        #         x0*x3 + x2*x3 + x0*y1 + y0*y1 + y3 + 1,
        #         x0*x3 + x2*x3 + x0*y1 + x1*y1 + x2*y1 + x0*y2 + y0*y2 + y2 + y3 + 1,
        #         x0*x2 + x0*x3 + x0*y0 + x1*y1 + x2*y1 + x0*y2 + y0*y3 + x3 + y3 + 1,
        #         x0*x2 + x2*x3 + y1*y2 + x2 + y1,
        #         x0*x3 + x2*x3 + y1*y3 + y1 + y3 + 1,
        #         x0*x2 + x0*x3 + x2*x3 + y2*y3 + x3]
    elif i == 3:
        return [x0 + y0*y3 + y0 + y1*y2*y3 + y1*y2 + y1*y3 + y2 + y3, 
                x1 + y0*y1*y2 + y0*y2*y3 + y0*y3 + y1*y2*y3 + y1*y2 + y1 + y2 + y3, 
                x2 + y0*y1*y3 + y0*y1 + y0*y2*y3 + y0*y2 + y0*y3 + y1*y2 + y1*y3 + y2*y3, 
                x3 + y0*y1*y2 + y0*y1*y3 + y0*y2 + y0*y3 + y0 + y1 + y2*y3 + y2]

        # return [x0*x2 + x0*x3 + x0*y1 + x0*y3 + x2 + x3 + y1 + y3,
        #         x0*x3 + x1*x3 + x0 + y1 + y2 + y3,
        #         x1*x2 + x0*x3 + x0*y0 + x1*y0 + x0*y1 + x0*y2 + x1 + y1 + y2 + y3,
        #         x1*x2 + x0*y0 + x1*y1 + x0*y2 + x1 + x2 + y0 + y3,
        #         x1*x2 + x0*y1 + x0*y2 + x1*y2,
        #         x0*x2 + x0*y0 + x1*y3 + x1 + x2 + y0 + y1 + y2,
        #         x0*x1 + x0*x3 + x0*y1 + x0*y2 + x2 + x3 + y1 + y3,
        #         x0*x2 + x1*x2 + x2*y0 + x0*y2 + x0 + x2 + y0 + y3,
        #         x0*x2 + x1*x2 + x0*x3 + x2*x3 + x0*y0 + x2*y1 + x0*y2,
        #         x1*x2 + x0*x3 + x2*x3 + x0*y0 + x2*y2 + x0 + y0 + y3,
        #         x1*x2 + x0*x3 + x2*x3 + x0*y2 + x2*y3 + x1 + x3 + y3,
        #         x3*y0 + x0*y2 + x0 + x1 + x2 + y1 + y2,
        #         x0*x2 + x0*x3 + x2*x3 + x3*y1 + x0*y2 + x2 + x3 + y2,
        #         x0*x2 + x0*x3 + x2*x3 + x0*y0 + x0*y2 + x3*y2 + x0 + x3 + y1 + y2 + y3,
        #         x0*x3 + x0*y0 + x3*y3 + x2 + y2,
        #         x0*y1 + y0*y1 + x0*y2 + x1 + x3 + y0 + y2 + y3,
        #         x0*x3 + y0*y2 + x0 + x1 + x2 + y1 + y2,
        #         x1*x2 + x0*x3 + x0*y1 + y0*y3 + x0 + x2 + y3,
        #         x0*y1 + x0*y2 + y1*y2 + x0 + x2 + y0 + y3,
        #         x0*x2 + x1*x2 + x2*x3 + y1*y3 + x1 + y1 + y2 + y3,
        #         x1*x2 + x0*x3 + x2*x3 + x0*y1 + y2*y3 + x1 + x2 + x3 + y2 + y3]
    elif i == 4:
        return [x0 + y0*y1*y3 + y0*y2*y3 + y0*y3 + y0 + y1 + y2*y3 + y2 + y3 + 1, 
                x1 + y0*y1 + y0*y2*y3 + y0*y2 + y0*y3 + y2 + y3, 
                x2 + y0*y1*y2 + y0*y1*y3 + y0*y1 + y0*y2 + y0 + y1*y3 + y1 + y2 + y3 + 1, 
                x3 + y0*y1*y3 + y0*y1 + y0*y3 + y1 + y2*y3 + y2]

        # return [x0*x1 + x0*x2 + x1*x2 + x0*y0 + x3 + y0 + y3 + 1,
        #         x0*x1 + x0*x3 + x1*x3 + x1 + x2 + x3 + y0 + 1,
        #         x1*y0 + x0 + x3 + y0 + y3 + 1,
        #         x0*x2 + x0*x3 + x0*y1 + x1*y1 + x3 + y0 + y3 + 1,
        #         x0*y0 + x1*y2 + x0*y3 + x1 + x2 + x3 + y0 + 1,
        #         x0*x3 + x1*y3 + x2 + x3 + y0 + 1,
        #         x0*x2 + x0*x3 + x0*y1 + x0*y2 + x2 + x3 + y1 + y2,
        #         x2*y0 + x0*y3 + x1 + x2 + y0 + y1 + 1,
        #         x0*x1 + x0*x2 + x0*y1 + x2*y1 + x0*y2 + x0*y3 + x3 + y0 + y3 + 1,
        #         x0*x2 + x2*x3 + x0*y1 + x2*y2 + x0*y3 + x0 + x2 + x3 + y0 + y3 + 1,
        #         x0*x1 + x0*x3 + x2*x3 + x0*y1 + x0*y3 + x2*y3 + x1 + x3 + y1 + y3,
        #         x0*x2 + x0*x3 + x2*x3 + x0*y0 + x3*y0 + x0,
        #         x0*x1 + x2*x3 + x3*y1 + x1 + x2 + y0 + 1,
        #         x0*x2 + x0*y0 + x0*y1 + x3*y2 + x0*y3 + x0 + x1 + x2 + x3 + y0 + 1,
        #         x0*x2 + x0*x3 + x0*y0 + x0*y1 + x0*y2 + x0*y3 + x3*y3 + x1 + x2 + x3 + y1 + y3,
        #         x0*x2 + x0*y1 + y0*y1 + x0*y2 + x3 + y0 + y3 + 1,
        #         x2*x3 + x0*y1 + x0*y2 + y0*y2 + x0 + x1 + x2 + x3 + y1 + y3,
        #         x0*x2 + x0*x3 + x2*x3 + y0*y3 + x3 + y0 + y3 + 1,
        #         x0*x1 + x2*x3 + x0*y0 + x0*y1 + x0*y2 + y1*y2 + x0*y3 + x0 + x1 + x2 + x3 + y1 + y3,
        #         x2*x3 + x0*y0 + x0*y1 + x0*y2 + x0*y3 + y1*y3 + x1 + x2 + y0 + y1 + 1,
        #         x0*y0 + y2*y3 + x0 + x1 + y0 + y1 + 1]
    elif i == 5:
        return [x0 + y0*y1*y3 + y0 + y1*y2 + y3, 
                x1 + y0*y1*y2 + y0*y1*y3 + y0*y2 + y0*y3 + y0 + y1*y2 + y1 + y3, 
                x2 + y0*y1*y3 + y0*y1 + y0*y2*y3 + y0 + y1*y3 + y2, 
                x3 + y0*y1*y2 + y0*y1 + y0*y3 + y1 + y2 + 1]

        # return [x0*x1 + x0*x2 + x0*x3 + x0*y3 + x0 + x1 + x2 + x3 + y3 + 1,
        #         x0*x1 + x0*x3 + x1*x3 + x1 + x2 + x3 + y0 + 1,
        #         x1*x2 + x0*x3 + x0*y0 + x1*y0 + x0*y3 + x1 + x2 + x3 + y3 + 1,
        #         x0*x1 + x0*x2 + x1*x2 + x0*y0 + x0*y1 + x1*y1 + x1 + x3 + y0 + y1 + y2 + 1,
        #         x0*x1 + x0*y1 + x0*y2 + x1*y2 + y0 + y3,
        #         x1*x2 + x0*y1 + x0*y2 + x1*y3 + y0 + y3,
        #         x0*x2 + x0*x3 + x2*x3 + x0*y0 + x1 + y0 + y1,
        #         x0*x1 + x0*x2 + x1*x2 + x0*y0 + x2*y0 + x0*y1 + x2 + y2 + y3,
        #         x0*x1 + x0*x3 + x0*y0 + x2*y1 + x0*y2 + x1 + x2 + y0 + y1 + y2 + y3,
        #         x0*x2 + x1*x2 + x0*x3 + x0*y0 + x0*y2 + x2*y2 + x0*y3 + x1 + y0 + y1 + y2 + y3,
        #         x1*x2 + x0*y0 + x0*y1 + x0*y3 + x2*y3 + x1 + y0 + y1,
        #         x0*x1 + x0*x2 + x0*x3 + x3*y0 + x0*y3 + x2 + x3 + y0 + y1 + y3 + 1,
        #         x0*x1 + x3*y1 + x1 + x2 + x3 + y0 + 1,
        #         x0*y1 + x3*y2 + x2 + y0 + y2,
        #         x0*x1 + x0*y1 + x0*y2 + x3*y3 + x2 + x3 + y1 + 1,
        #         x0*x1 + x0*x2 + x1*x2 + y0*y1 + x1 + y1,
        #         x1*x2 + x0*x3 + y0*y2 + x0*y3 + x1 + x2 + y1 + y2 + y3,
        #         x0*x2 + x0*x3 + x0*y3 + y0*y3 + x3 + y1 + y2 + 1,
        #         x1*x2 + x0*x3 + x0*y0 + y1*y2 + x0*y3 + x2 + y2,
        #         x1*x2 + x0*x3 + x0*y1 + x0*y3 + y1*y3 + x2 + x3 + y0 + y1 + 1,
        #         x0*x2 + x1*x2 + x0*x3 + x0*y1 + y2*y3 + x1 + x2 + y0 + y1 + y2]
    elif i == 6:
        return [x0 + y0*y1*y2 + y0*y1*y3 + y0*y1 + y0*y2 + y0 + y1*y2*y3 + y1*y2 + y3 + 1, 
                x1 + y0*y2 + y1 + y2 + y3 + 1, 
                x2 + y0*y1*y3 + y0 + y1*y2*y3 + y1*y2 + y1*y3 + y1 + y2*y3 + 1, 
                x3 + y0*y1*y2 + y0*y1*y3 + y0*y1 + y0*y3 + y1*y2*y3 + y1*y2 + y1 + y2*y3 + y2 + y3 + 1]

        # return [x0*x3 + x1 + x2 + y1 + 1,
        #         x0*x1 + x0*x2 + x0*x3 + x0*y1 + x0,
        #         x1*x2 + x1*x3 + x1*y0 + x0*y2 + x0*y3 + x0 + x3 + y0 + y1,
        #         x0*x2 + x0*x3 + x1*x3 + x1*y1 + x0 + x2 + x3 + y1 + y2 + y3,
        #         x0*x2 + x1*x2 + x0*y1 + x0*y2 + x1*y2 + x0*y3 + x3 + y2 + y3 + 1,
        #         x0*x2 + x1*x2 + x1*x3 + x0*y1 + x1*y3 + x0 + x2 + y1 + 1,
        #         x1*x3 + x2*x3 + x0*y1 + x0 + x2 + x3 + y0 + y1 + y2 + 1,
        #         x1*x2 + x0*x3 + x1*x3 + x0*y0 + x2*y0 + x0 + x2 + y2 + 1,
        #         x1*x3 + x0*y0 + x2*y1 + x0*y2 + x0 + x2 + x3 + y1 + y2 + y3,
        #         x0*x2 + x0*x3 + x0*y0 + x2*y2 + x0 + y0 + y3 + 1,
        #         x0*x2 + x1*x2 + x1*x3 + x0*y1 + x0*y2 + x0*y3 + x2*y3 + x0 + y0 + y2 + y3,
        #         x0*x2 + x1*x2 + x0*x3 + x1*x3 + x3*y0 + x0*y2 + x0 + y0 + y3 + 1,
        #         x0*x3 + x0*y1 + x3*y1 + x0 + x2 + y0 + y1 + y2 + 1,
        #         x1*x2 + x0*y0 + x0*y1 + x3*y2 + x2 + y1 + y2 + y3,
        #         x0*x2 + x1*x2 + x1*x3 + x0*y3 + x3*y3 + x0 + y0 + y1,
        #         x1*x2 + x0*x3 + x1*x3 + x0*y1 + y0*y1 + x0*y2 + x0*y3 + y3 + 1,
        #         x0*x3 + y0*y2 + x2 + y2 + y3,
        #         x0*x3 + x0*y1 + y0*y3 + x0 + x3 + y1 + y3 + 1,
        #         x0*x2 + x1*x2 + x0*x3 + x0*y2 + y1*y2 + x0*y3 + x3 + y0,
        #         x1*x2 + x0*x3 + x1*x3 + x0*y0 + x0*y3 + y1*y3 + x0 + x3 + y0 + y3 + 1,
        #         x0*y1 + y2*y3 + x2 + y0 + 1]
    elif i == 7:
        return [x0 + y0*y1*y3 + y0 + y1*y2*y3 + y1*y2 + y1*y3 + y1 + y2*y3 + 1, 
                x1 + y0*y2*y3 + y0*y3 + y0 + y1*y2*y3 + y1*y2 + y1*y3 + y2 + y3 + 1, 
                x2 + y0*y1*y3 + y0*y2*y3 + y0*y2 + y1 + y2*y3 + y3, 
                x3 + y0*y1*y2 + y0*y1*y3 + y0*y1 + y0*y3 + y1*y3 + y2]

        # return [x0*x1 + x0*x2 + x0*y3 + x1 + x2 + y3,
        #         x0*x1 + x1*x2 + x0*y1 + x0 + x3 + y1 + y3,
        #         x0*x1 + x0*x2 + x0*x3 + x1*x3 + x0*y0 + x1*y0 + x0*y1 + x1 + x3 + y1 + y3,
        #         x0*x1 + x0*x2 + x1*x2 + x1*x3 + x0*y1 + x1*y1 + x0*y2 + x1 + x3 + y1 + y3,
        #         x0*x1 + x0*x2 + x1*x2 + x0*x3 + x1*x3 + x0*y0 + x0*y2 + x1*y2 + x1 + x3 + y2 + y3,
        #         x0*x2 + x0*y0 + x0*y1 + x0*y2 + x1*y3 + x2 + x3 + y1,
        #         x1*x2 + x2*x3 + x1 + y0 + y1 + y2 + y3 + 1,
        #         x0*x1 + x0*x2 + x0*x3 + x2*y0 + x0*y1 + x2 + x3 + y0 + y2 + y3 + 1,
        #         x0*y1 + x2*y1 + x0*y2 + x1 + x2 + x3 + y0 + y2 + 1,
        #         x1*x2 + x0*x3 + x0*y0 + x2*y2 + x1 + x2 + x3 + y0 + y2 + 1,
        #         x0*x1 + x0*x2 + x0*y0 + x0*y2 + x2*y3 + x1 + x3 + y1,
        #         x0*x2 + x0*y0 + x3*y0 + x0*y1 + x0*y2 + y2 + y3,
        #         x0*x1 + x0*x3 + x0*y0 + x0*y1 + x3*y1 + x0*y2 + x2 + x3 + y0 + 1,
        #         x0*x1 + x0*x2 + x1*x2 + x0*x3 + x1*x3 + x0*y2 + x3*y2 + x2 + y0 + y1 + y3 + 1,
        #         x0*x1 + x1*x2 + x1*x3 + x0*y0 + x0*y1 + x3*y3 + x2 + y0 + y1 + y2 + 1,
        #         x0*x1 + x0*x3 + y0*y1 + x3 + y0 + y1 + y2 + y3 + 1,
        #         x0*x1 + x1*x2 + x1*x3 + x0*y1 + y0*y2 + x3 + y0 + y3 + 1,
        #         x1*x3 + y0*y3 + x1 + x2 + y0 + y1 + y2 + 1,
        #         x0*x2 + x1*x2 + x0*x3 + x1*x3 + x0*y0 + x0*y1 + x0*y2 + y1*y2 + x3 + y1 + y2 + y3,
        #         x1*x3 + x0*y0 + x0*y2 + y1*y3 + x1 + x3 + y0 + y2 + y3 + 1,
        #         x1*x2 + x1*x3 + y2*y3 + x1 + x3 + y0 + y1 + y3 + 1]


load("serpent.spyx")    

def test_serpent_mip(nwords=4, delta0=0.15, delta1=0.001, conversion='IASC', solver=None, coldboot=True, write_out=False, timeout=None, parameters=None, shift=0):
    
    ctx = SerpentColdboot(nwords=nwords+shift)
    K = ctx.random_key()
    F,s = ctx.polynomial_system(K=K, delta0=delta0, delta1=delta1)
    H, S = F.hard_generators(), F.soft_generators()

    S = S[32*shift:]
    
    if coldboot == 'aggressive':
        H += tuple([f for f in S if f.constant_coefficient() == 1])
        S = tuple([f for f in S if f.constant_coefficient() == 0])

        F = ProbabilisticMPolynomialSystem(F.ring(),hard=H,soft=S)

    if not delta1 or not delta0:
        dd = 1
    else:
        dd = int(delta0/delta1)

    def weight_callback(f):
        if f.constant_coefficient() == 1:
            return dd # the probability that this is wrong is
                      # small, so the penalty is big
        else:
            return 1

    t = walltime()
    if write_out:
        F.write_mip("/home/malb/Serpent-%s-%4.2f.mps"%(nwords,delta0),conversion=conversion,
                    weight_callback=weight_callback,
                    filetype='mps')
        return walltime(t)
        
    if solver != "SCIP2":
        S = F.solve_mip(conversion=conversion,
                        solver=solver,
                        weight_callback=weight_callback,
                        timeout=timeout)

    else:
        S = F.solve_scip(timeout=timeout, parameters=parameters, weight_callback=weight_callback)
    t = walltime(t)

    if S is None:
        return t, -1.0, False

    for s,obj in S:
        correct = True
        for i,v in enumerate(ctx.K_var):
            try:
                if s[v]  != K[i]:
                    correct = False
                    break
            except KeyError:
                pass
        if correct:
            return t, obj, correct
    return t, -2.0, False
